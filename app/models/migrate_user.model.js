module.exports = (sequelize, Sequelize) => {
    const register = sequelize.define("register", {
      mobile: {
        type: Sequelize.NUMBER
      },
      description: {
        type: Sequelize.STRING
      },
      published: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return register;
  };